## 没有最坑，只有更坑！

![bullshit](resource/bullshit.png)

### 活动简介

作为一个程序员，你看过哪些坑爹代码，你又写过多少坑爹代码，还有多少你不知道的坑爹代码？本仓库的目的就是为了收集这些坑爹代码，可以让别人不掉坑或者少掉坑，可以避免自己掉坑，或许哈哈一乐。如果你觉得这个仓库有意思，别忘了给个 Star 哦。


### 提交作品

任何人都可以提交代码片段，提交人请 fork 此仓库到个人仓库，完成自己的代码后，通过提交 Pull Requests 来提交作品。 

关于 Fork 和 Pull Requests 的用法请阅读 [https://gitee.com/help/articles/4128](https://gitee.com/help/articles/4128)

如何提交坑爹代码：

1. 可以是任何编程语言
2. 请给代码文件取一个有意义的文件名，文件名长度不得超过 20 个字符（不含扩展名），不能和他人的文件重名（请根据不同的语言存放到对应目录）
3. 提交信息中请写明代码的坑爹之处
4. 允许提交多个作品
5. 每段代码请务必给出详细的注释和说明

具体形式可参考作品 [Demo.java](java/Demo.java)